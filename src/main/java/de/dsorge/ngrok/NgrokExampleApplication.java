package de.dsorge.ngrok;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NgrokExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(NgrokExampleApplication.class, args);
	}

}
