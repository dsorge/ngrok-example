package de.dsorge.ngrok;

import lombok.AllArgsConstructor;
import ngrok.api.NgrokApiClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class HelloWorldController {

    private final NgrokApiClient ngrokApiClient;

    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public String sayHello(){
        return "{'hello':'world'}";
    }

    @GetMapping(value="/url", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getUrl(){
        return "{'url':'"+ngrokApiClient.getHttpsTunnelUrl()+"'}";
    }
}
